# springcloud-chapter06-1

#### 介绍
第六章服务网关Zuul 
P83  

#### 软件架构
软件架构说明


#### 安装教程

Zuul pom.xml
```xml
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-zuul</artifactId>
		</dependency>
```

application.yml
```yaml
server:
    port: 8835
spring :
  application :
    name : gateway-zuul
eureka :
  client :
    service-url:
      defaultZone: http://localhost:7000/eureka/
zuul:
  routes:
    eureka-consumer:
       path: /eureka-consumer/**

```
#### 使用说明

![](./doc/1-1.png)

![](./doc/1-2.png)

