package com.itheima.eurekaconsumer.service;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Service
@FeignClient(value = "eureka-provider",fallback = LocalItemServiceImpl.class)
public interface LocalItemService {
    @RequestMapping(value="/hi",method = RequestMethod.GET)
    public String hi(@RequestParam(value = "id") String id);

    @RequestMapping(value="/hello",method = RequestMethod.GET)
    public String hello(@RequestParam(value = "id") String id);
}
