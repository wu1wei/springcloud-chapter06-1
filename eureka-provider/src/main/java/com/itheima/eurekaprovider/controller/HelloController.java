package com.itheima.eurekaprovider.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by itcast on 2019/10/23.
 */
@RestController
public class HelloController {
    @RequestMapping("/hi")
    public String hi(String id){
        String mid = "[学号:姓名:机器名]";
        return "Hello World, I'm from hello provider!"+mid+id;
    }

}
